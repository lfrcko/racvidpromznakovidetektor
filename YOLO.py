import cv2
import numpy as np

# Global variables
current_frame = 0
frame_step = 1

# Function to update the current frame
def update_frame_position(position):
    global current_frame
    current_frame = position

# Load video file
video_path = "path/to/video.mp4"
cap = cv2.VideoCapture('DayDrive1.mp4')
# Get total number of frames in the video
total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

# Load YOLOv4 model
net = cv2.dnn.readNetFromDarknet('yolov4.cfg', 'yolov4.weights')
layer_names = net.getLayerNames()
output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
class_names = []

# Read class names from file
with open('coco.names', 'r') as f:
    class_names = [line.strip() for line in f.readlines()]

cv2.namedWindow("Video")
cv2.createTrackbar("Position", "Video", 0, total_frames-1, update_frame_position)

# Process video frames
while cap.isOpened():
    # Set the video capture to the current frame position
    cap.set(cv2.CAP_PROP_POS_FRAMES, current_frame)

    # Read the frame
    ret, frame = cap.read()
    if not ret:
        break

    height, width, _ = frame.shape

    # Perform object detection using YOLOv4
    blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416), swapRB=True, crop=False)
    net.setInput(blob)
    layer_outputs = net.forward(output_layers)

    # Process the outputs and draw bounding boxes for traffic signs
    for output in layer_outputs:
        for detection in output:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5 and class_names[class_id] == 'traffic sign':
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                bbox_width = int(detection[2] * width)
                bbox_height = int(detection[3] * height)
                x = int(center_x - bbox_width / 2)
                y = int(center_y - bbox_height / 2)

                # Draw the bounding box and label
                cv2.rectangle(frame, (x, y), (x + bbox_width, y + bbox_height), (0, 255, 0), 2)
                cv2.putText(frame, class_names[class_id], (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

    height, width, _ = frame.shape
    new_height = int(height * 0.7)  # 70% of the original height
    cropped_frame = frame[:new_height, :]

    # Display cropped frame
    cv2.imshow("Video", cropped_frame)

    # Check for keyboard events
    key = cv2.waitKey(1)
    if key & 0xFF == ord('q'):
        break
    elif key == ord('n'):  # Button for forward 10 frames
        current_frame += 10
        current_frame = min(current_frame, total_frames-1)
        cv2.setTrackbarPos("Position", "Video", current_frame)
    elif key == ord('m'):  # Button for forward 20 frames
        current_frame += 20
        current_frame = min(current_frame, total_frames-1)
        cv2.setTrackbarPos("Position", "Video", current_frame)
    elif key == ord(','):  # Button for forward 30 frames
        current_frame += 30
        current_frame = min(current_frame, total_frames-1)
        cv2.setTrackbarPos("Position", "Video", current_frame)
    elif key == ord('.'):  # Button for backward 30 frames
        current_frame -= 30
        current_frame = min(current_frame, total_frames-1)
        cv2.setTrackbarPos("Position", "Video", current_frame)

# Release resources
cap.release()
cv2.destroyAllWindows()